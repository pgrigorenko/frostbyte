grammar Frostbyte;

import CommonLexerRules;

options {
  language = Java;
}

statements
    : (statement NEWLINE)*
    ;

statement
    : function
    | chunk
    ;

function
    : 'fun' IDENTIFIER ':=' functionBody
    ;

functionBody
    : body = block
    ;

chunk
    : 'chunk' IDENTIFIER ':=' chunkBody = block
    ;

call
    : 'call' IDENTIFIER argument*
    ;

argument
    : STRING
    | paramPlaceholder
    | block
    ;

block
    : '(' (call | with | get) ')'
    ;

with
    : 'with' IDENTIFIER? argument*
    ;

get
    : 'get' IDENTIFIER
    ;

paramPlaceholder : '...' ;
