package org.zt.frostbyte.parser;

import org.antlr.v4.runtime.misc.NotNull;
import org.zt.frostbyte.parser.generated.FrostbyteBaseListener;
import org.zt.frostbyte.parser.generated.FrostbyteParser;

/**
 * @author Pavel Grigorenko
 */
public class ParserListenerImpl extends FrostbyteBaseListener {

  String id;
  String body;

  @Override
  public void enterFunction(@NotNull FrostbyteParser.FunctionContext ctx) {
    id = ctx.IDENTIFIER().getText();
  }

  @Override
  public void enterFunctionBody(@NotNull FrostbyteParser.FunctionBodyContext ctx) {
    body = ctx.body.getText();
  }

  @Override
  public void exitFunction(@NotNull FrostbyteParser.FunctionContext ctx) {
    System.out.println("fun " + id + " -> " + body);
  }

  @Override
  public void enterChunk(FrostbyteParser.ChunkContext ctx) {
    System.out.println("chunk " + ctx.IDENTIFIER() + " -> " + ctx.chunkBody.getText());
  }
}
