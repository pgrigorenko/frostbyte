package org.zt.frostbyte.parser;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.zt.frostbyte.parser.generated.FrostbyteLexer;
import org.zt.frostbyte.parser.generated.FrostbyteParser;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * @author Pavel Grigorenko
 */
public class Loader {

  public static void main(String[] args) throws IOException, URISyntaxException {
    load(new ANTLRFileStream(new File(Loader.class.getClassLoader().getResource(args[0]).toURI()).getAbsolutePath()));

//    String s =
//            "fun main := (call echo \"Hello World!\")\n" +
//            "fun mytest := (blah blah)\n" +
//            "chunk echo := (with System (with (get out) (call println ...)))\n";
//
//    load(s);
  }

  private static void load(String input) throws IOException {
    load(new ANTLRInputStream(input));
  }

  private static void load(CharStream input) throws IOException {
    final FrostbyteLexer lexer = new FrostbyteLexer(input);
    TokenStream token = new CommonTokenStream(lexer);
    final FrostbyteParser parser = new FrostbyteParser(token);
//    System.out.println(parser.statements().toStringTree());
    ParseTreeWalker walker = new ParseTreeWalker();
    walker.walk(new ParserListenerImpl(), parser.statements());
  }
}
