lexer grammar CommonLexerRules;

IDENTIFIER
    : LETTER LETTER_OR_DNUMBER*
    ;

fragment LETTER : [a-zA-Z$_];
fragment LETTER_OR_DNUMBER : [a-zA-Z0-9$_];

STRING: '"' (ESC|.)*? '"' ;

fragment
ESC : '\\"' | '\\\\' ; // 2-char sequences \" and \\

NEWLINE: '\r'? '\n' ;

WS  :  ([ \t\r\n\u000C]+ | EOF) -> skip
    ;

COMMENT
    :   '/*' .*? '*/' -> skip
    ;

LINE_COMMENT
    :   '//' ~[\r\n]* -> skip
    ;
